#include "trajectory_generation.h"
#include <iostream>

#include <iostream>
using namespace std;

int main(){

const double tf = 2.0;
const double dt = 0.1;
Eigen::Vector2d xi(1, 0);
Eigen::Vector2d xf(3, 4);
Eigen::Vector2d X;
Eigen::Vector2d V;
Eigen::Vector2d A;

Point2Point Traj(xi, xf, tf); //Creation of "Traj" to get all the points for the trajectory



cout << "t x y vx vy ax ay" << endl;

for (double i = 0.0; i <= tf; i = i + dt){ //Print X, V and A for all instances
  
  X = Traj.X(i);
  V = Traj.dX(i);
  A = Traj.ddX(i);

  cout << i << " " << X(0) << " " << X(1) << " " << V(0) << " " << V(1) << " " << A(0) << " " << A(1) << endl;
}


  // Compute the trajectory for given initial and final positions. 
  // Display some of the computed points
  // Check numerically the velocities given by the library 
  // Check initial and final velocities
}