#include "control.h"
#include "trajectory_generation.h"
#include <iostream>

int main(){
  
  Eigen::Vector2d xi {Eigen::Vector2d::Zero()};
  Eigen::Vector2d xf(0, 0.6);
  Eigen::Vector2d q(M_PI_2, M_PI_4);
  Eigen::Matrix2d J;
  Eigen::Vector2d dqd {Eigen::Vector2d::Zero()};
  Eigen::Vector2d Xd {Eigen::Vector2d::Zero()};
  Eigen::Vector2d dXd {Eigen::Vector2d::Zero()};
  Eigen::Vector2d Xa {Eigen::Vector2d::Zero()};

  const double dt = 0.01; 
  const double tf = 10.0;

  RobotModel rm(0.4, 0.5); //Robot dimension
  Controller control(rm); //Object control with dimension of rm


  rm.FwdKin(xi, J, q); //Output xi and J, we used xi for the next step (to get initial position)
  Point2Point Traj(xi, xf, tf);

  std::cout<< "t xd yd xa ya" << std::endl; //Output variable

  for (double i = 0.0; i <= tf; i = i + dt){ 
    Xd = Traj.X(i); //X desired
    dXd = Traj.dX(i); //Velocity desired

    dqd = control.Dqd(q, Xd, dXd); //Velocity of all joints
    q = q + dqd*dt; //Position of all joint
    rm.FwdKin(Xa, J, q); //"sensor" to see actual position of the robot
    std::cout << i << " " << Xd(0) << " " << Xd(1) << " " << Xa(0) << " " << Xa(1) << std::endl;
    

    
  }
}