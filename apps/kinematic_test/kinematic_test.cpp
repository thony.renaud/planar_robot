#include "kinematic_model.h"
#include <iostream>
using namespace std;

int main(){
  // Compute the forward kinematics and jacobian matrix for 
  //      q =  M_PI/3.0,  M_PI/4
  // For a small variation of q, compute the variation on X and check dx = J . dq 
  Eigen::Vector2d x; // initial vector
  Eigen::Matrix2d J; // initial Jacobian
  Eigen::Vector2d x1; // variation
  Eigen::Matrix2d J1; // variation
  Eigen::Vector2d xdiff;
  Eigen::Vector2d xdifftest;
  Eigen::Vector2d q(0, M_PI/4.0);
  Eigen::Vector2d q1(0.01, (M_PI/4.0) + 0.01);
  Eigen::Vector2d q2(0.01, 0.01);

  //Get X and J with q
  RobotModel rm(0.5, 0.5);
  rm.FwdKin(x, J, q);
  cout << x << endl;
  cout << J << endl;
  cout << endl;
  
  //We want to see if delta x = J* delta q
  RobotModel rm1(0.5, 0.5);
  rm.FwdKin(x1, J1, q1);
  cout << x1 << endl;
  cout << J1 << endl;

  xdiff = x1 - x;
  cout << endl;
  cout << xdiff << endl;

  xdifftest = J1 * q2;
  cout << xdifftest << endl;


}
