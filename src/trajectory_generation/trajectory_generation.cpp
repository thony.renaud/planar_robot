#include "trajectory_generation.h"
#include <iostream>
using namespace std;

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //Update all coefficients
  update(piIn, pfIn, DtIn);
  
  };

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //Get all coefficient each time
 
  a[0] = piIn;
  a[1] = 0;
  a[2] = 0;
  a[3] = (10/(pow(DtIn, 3)))*(pfIn - piIn);
  a[4] = (-15/(pow(DtIn, 4)))*(pfIn - piIn);
  a[5] = (6/(pow(DtIn, 5)))*(pfIn - piIn);
};

const double  Polynomial::p     (const double &t){
  //Compute position
  const double p = a[0] + a[1]*t + a[2]*pow(t, 2) + a[3]*pow(t, 3) + a[4]*pow(t, 4) + a[5]*pow(t, 5);
  return p;
};

const double  Polynomial::dp    (const double &t){
  //Compute velocity
  const double dp = a[1] + 2*a[2]*t + 3*a[3]*pow(t, 2) + 4*a[4]*pow(t, 3) + 5*a[5]*pow(t, 4);
  return dp;
};

const double Polynomial::ddp    (const double &t){
  //Compute acceleration
  const double ddp = 2*a[2] + 6*a[3]*t + 12*a[4]*pow(t, 2) + 20*a[5]*pow(t, 3);
  return ddp;
};

Point2Point::Point2Point(const Eigen::Vector2d & xi, const Eigen::Vector2d & xf, const double & DtIn){
  //Initialize object and polynomials
  polx.update(xi(0), xf(0), DtIn);
  poly.update(xi(1), xf(1), DtIn);
}

Eigen::Vector2d Point2Point::X(const double & time){
  //Cartesian position
  Eigen::Vector2d XD;

  XD(0) = polx.p(time);
  XD(1) = poly.p(time);

  return XD;
}

Eigen::Vector2d Point2Point::dX(const double & time){
  //Cartesian velocity
  Eigen::Vector2d VD;

  VD(0) = polx.dp(time);
  VD(1) = poly.dp(time);

  return VD;
}

  Eigen::Vector2d Point2Point::ddX(const double & time){
  //Cartessian acceleration
  Eigen::Vector2d AD;

  AD(0) = polx.ddp(time);
  AD(1) = poly.ddp(time);

  return AD;

}