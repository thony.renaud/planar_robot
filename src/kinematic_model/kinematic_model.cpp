#include "kinematic_model.h"
using namespace std;

RobotModel::RobotModel(const double &l1In, const double &l2In):
  l1  (l1In), //Get dimension of the robot
  l2  (l2In)
{};

void RobotModel::FwdKin(Eigen::Vector2d &xOut, Eigen::Matrix2d &JOut, const Eigen::Vector2d & qIn){
  //Calcul Xout with actual q
  xOut(0) = l1*cos(qIn(0)) + l2*cos(qIn(0) + qIn(1));
  xOut(1) = l1*sin(qIn(0)) + l2*sin(qIn(0) + qIn(1));


  //Calcul Jout with actual q
  JOut(0, 0) = -l1*sin(qIn(0)) - l2*sin(qIn(0) + qIn(1));
  JOut(0, 1) = -l2*sin(qIn(0) + qIn(1));
  JOut(1, 0) = l1*cos(qIn(0)) + l2*cos(qIn(0) + qIn(1));
  JOut(1, 1) = l2*cos(qIn(0) + qIn(1));
}